[[_TOC_]]

# k1-hcloud

Terraform & k1 for provisioning k8s on HCLOUD.

## Terraform
```bash
# Basic run with local state file
terraform init
# create and view an execution plan
terraform plan
# apply the changes
terraform apply -auto-approve
# output the apply
terraform output -json > tf.json
# kubeone
ssh-add
kubeone apply -m kubeone.yaml -t tf.json
```

## just
```bash
# runs all TF stages
just cluster-create
```